# Generated by Django 3.0.7 on 2020-08-06 08:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blogs', '0009_auto_20200806_1327'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='blog',
            options={'ordering': ('-date',)},
        ),
    ]
