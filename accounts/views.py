from django.shortcuts import render
from django.contrib.auth.models import User
from django.http.response import HttpResponseRedirect
from django.contrib.auth import authenticate,login,logout as auth_logout
# Create your views here.
def signup(request):
    if request.method=="POST":
        username=request.POST['username']
        email=request.POST['e-mail']
        password=request.POST['password']
        user=User.objects.create_user(username=username,password=password,email=email)
        return HttpResponseRedirect("/")
    return render(request,'signup.html',{})

def signin(request):
    if request.method=="POST":
        
        email=request.POST['e-mail']
        password=request.POST['password']
        user =authenticate(request,email=email,password=password)
        login(request,user,backend='django.contrib.auth.backends.ModelBackend')
        if user is not None:
            return HttpResponseRedirect('/')
        

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/')
