from django.apps import AppConfig


class HexanovateConfig(AppConfig):
    name = 'hexanovate'
