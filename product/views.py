from django.shortcuts import render
from .models import Product
from blogs.models import Blog
# Create your views here.


def ProductView(request):
   products= Product.objects.all()
   context={}
   queryset = Blog.objects.filter(active=True)
   context['recent_blogs'] = queryset.order_by('-date')[:2]
   context['products']=products
   return render(request,'product.html',context)

