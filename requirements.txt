Django
django-heroku
dj-database-url==0.5.0
django-cors-headers==3.4.0
django-crispy-forms==1.9.1
django-db-connection-pool==1.0.2
djangorestframework==3.11.0
django_ckeditor_5
gunicorn==20.0.4
Pillow==7.1.2
whitenoise==5.1.0
django-allauth==0.42.0
django-social-share==1.4.0
django-summernote


